<div class="toggle-nav">
  <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
</div>
<!--logo start-->
<a href="<?php echo base_url('admin') ?>" class="logo">Akse<span class="lite">soris</span></a>
<!--logo end-->
<div class="nav search-row" id="top_menu">
  <!--  search form start -->
  <ul class="nav top-menu">
    <li>
      <form action="<?php echo base_url('item/search') ?>" method="get" class="navbar-form">
        <input class="form-control" name="keyword" placeholder="Search" type="text">
        <button type="submit" class="btn btn-default">Search</button>
      </form>
    </li>
  </ul>
  <!--  search form end -->
</div>
<div class="top-nav notification-row">
  <!-- notificatoin dropdown start-->
  <ul class="nav pull-right top-menu">
    <!-- user login dropdown start-->
    <li class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <i class="fa fa-shopping-cart"></i>
        <span class="username">Admin</span>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu extended logout">
        <div class="log-arrow-up"></div>
        <li class="eborder-top">
          <a href="#"><i class="icon_profile"></i> My Profile</a>
        </li>
        <li>
          <a href="production/login.html"><i class="icon_key_alt"></i> Log Out</a>
        </li>
      </ul>
    </li>
    <!-- user login dropdown end -->
  </ul>
  <!-- notificatoin dropdown end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
<div id="sidebar" class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu">
    <li class="active">
      <a class="" href="<?php echo base_url('admin') ?>">
        <i class="icon_house_alt"></i>
        <span>Aksesoris</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>item/add_item" class="">
        <i class="icon_document_alt"></i>
        <span>Kelola Aksesoris</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="javascript:;" class="">
        <i class="icon_desktop"></i>
        <span>UI Fitures</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="general.html">Elements</a></li>
        <li><a class="" href="buttons.html">Buttons</a></li>
        <li><a class="" href="grids.html">Grids</a></li>
      </ul>
    </li>
    <li>
      <a class="" href="widgets.html">
        <i class="icon_genius"></i>
        <span>Widgets</span>
      </a>
    </li>
    <li>
      <a class="" href="chart-chartjs.html">
        <i class="icon_piechart"></i>
        <span>Charts</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="javascript:;" class="">
        <i class="icon_table"></i>
        <span>Tables</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="basic_table.html">Basic Table</a></li>
      </ul>
    </li>
    <li class="sub-menu">
      <a href="javascript:;" class="">
        <i class="icon_documents_alt"></i>
        <span>Pages</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="profile.html">Profile</a></li>
        <li><a class="" href="login.html"><span>Login Page</span></a></li>
        <li><a class="" href="contact.html"><span>Contact Page</span></a></li>
        <li><a class="" href="blank.html">Blank Page</a></li>
        <li><a class="" href="404.html">404 Error</a></li>
      </ul>
    </li>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->