<div class="x_panel">
    <div class="x_title">
        <h2>Detail Aksesoris</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <img src="<?php echo base_url(); ?>/asset/upload/<?= $gambar;?>" alt="" style="width: 100%">
            </div>
            <div class="col-md-6 col-sm-6">
                <table class="table table-striped">
                    <tr>
                        <td width="100px">Merk</td>
                        <td>: <?php echo $merk; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Jenis</td>
                        <td>: <?php echo $jenis; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Bahan</td>
                        <td>: <?php echo $bahan; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Harga</td>
                        <td>: <?php echo 'Rp' . number_format($harga_item, 0, ',','.'); ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Stok</td>
                        <td>: <?php echo $stok; ?></td>
                    </tr>
                    <tr>
                        <td width="100px">Deskripsi</td>
                        <td>: <?php echo nl2br($deskripsi); ?></td>
                    </tr>
                </table>
                <a href="#" class="btn btn-primary" onclick="window.history.go(-1)">Kembali</a>
            </div>
        </div>
    </div>
</div>
