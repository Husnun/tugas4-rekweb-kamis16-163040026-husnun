<div class="toggle-nav">
  <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
</div>
<!--logo start-->
<a href="<?php echo base_url('customer') ?>" class="logo">Aksesoris <span class="lite">Wanita</span></a>
<!--logo end-->
<div class="nav search-row" id="top_menu">
  <!--  search form start -->
  <ul class="nav top-menu">
    <li>
      <form action="<?php echo base_url('customer/cari') ?>" method="get" class="navbar-form">
        <input class="form-control" placeholder="Search" type="text">
      </form>
    </li>
  </ul>
  <!--  search form end -->
</div>
<div class="top-nav notification-row">
  <!-- notificatoin dropdown start-->
  <ul class="nav pull-right top-menu">
    <!-- user login dropdown start-->
    <li class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <i class="icon_profile"></i>
        <span class="username">Customer</span>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu extended logout">
        <div class="log-arrow-up"></div>
        <li class="eborder-top">
          <a href="#"><i class="icon_profile"></i> My Profile</a>
        </li>
        <li>
          <a href="production/login.html"><i class="icon_key_alt"></i> Log Out</a>
        </li>
      </ul>
    </li>
    <!-- user login dropdown end -->
  </ul>
  <!-- notificatoin dropdown end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
<div id="sidebar" class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu">
    <li class="active">
      <a class="" href="<?php echo base_url('customer') ?>">
        <i class="icon_house_alt"></i>
        <span>Aksesoris</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <i class="icon_document_alt"></i>
        <span>Perhiasan</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <span>Jam Tangan</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <span>Tas</span>
      </a>
    </li>
    <li class="sub-menu">
      <a href="<?php echo base_url();?>" class="">
        <span>Dompet</span>
      </a>
    </li>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->