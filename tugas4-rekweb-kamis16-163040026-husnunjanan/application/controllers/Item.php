<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('app_admin');
        $this->API = 'http://localhost/rest_server';
    }


    public function index() {
        $this->template->admin('admin/dashboard', 'admin/manage_item');
    }

    public function add_item() {
        if($this->input->post('submit', TRUE) == 'submit') {
            //validasi input
            $this->form_validation->set_rules('merk', 'Merk Item', 'required|min_length[4]');
            $this->form_validation->set_rules('jenis', 'Jenis Item', 'required|min_length[4]');
            $this->form_validation->set_rules('bahan', 'Bahan Item', 'required|min_length[4]');
            $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
            $this->form_validation->set_rules('stok', 'Jumlah Stok', 'required|numeric');
            $this->form_validation->set_rules('deskripsi', 'Deskripsi Item', 'required|min_length[4]');


            if ($this->form_validation->run() == TRUE) {
                $config['upload_path'] = './asset/upload';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['file_name'] = 'gambar' . time();

                $this->load->library('upload', $config);

                if($this->upload->do_upload('foto')) {
                    $gbr = $this->upload->data();
                    //proses insert
                    $data = array(
                        'merk' => $this->input->post('merk', TRUE),
                        'jenis' => $this->input->post('jenis', TRUE),
                        'bahan' => $this->input->post('bahan', TRUE),
                        'harga' => $this->input->post('harga', TRUE),
                        'stok' => $this->input->post('stok', TRUE),
                        'gambar' => $gbr['file_name'],
                        'deskripsi' => $this->input->post('deskripsi', TRUE)
                );
                    $this->curl->simple_post($this->API . '/Adminapi/', $data, array(CURLOPT_BUFFERSIZE => 10));
                     $this->session->set_flashdata('insert', 'Data berhasil ditambahkan!');
                    redirect('item');
                } else {
                    $this->session->set_flashdata('alert', 'Masukkan foto produk!');
                }
            }
        }
        $data['header'] = "Tambah Aksesoris";
        $data['merk'] = $this->input->post('merk', TRUE);
        $data['jenis'] = $this->input->post('jenis', TRUE);
        $data['bahan'] = $this->input->post('bahan', TRUE);
        $data['harga'] = $this->input->post('harga', TRUE);
        $data['stok'] = $this->input->post('stok', TRUE);
        $data['deskripsi'] = $this->input->post('deskripsi', TRUE);

      
        $this->template->admin('admin/dashboard', 'admin/item_form', $data);
    }

    public function delete($id) {
        json_decode($this->curl->simple_delete($this->API . '/Adminapi/', array('id' => $id), array(CURLOPT_BUFFERSIZE => 10)));
        $this->session->set_flashdata('alert', 'Data berhasil dihapus!');
        redirect('item');
    }

    public function detail() {
        $id_item = $this->uri->segment(3);
        $items = json_decode($this->curl->simple_get($this->API . '/Adminapi/', array("id" => $id)));

        foreach ($items as $key) {
            $data['merk'] = $key->merk;
            $data['jenis'] = $key->jenis;
            $data['bahan'] = $key->bahan;
            $data['harga'] = $key->harga;
            $data['stok'] = $key->stok;
            $data['deskripsi'] = $key->deskripsi;
            $data['gambar'] = $key->gambar;

        }
        $this->template->admin('admin/dashboard', 'admin/detail_item', $data);
    }

    public function update_item() {
        $id_item = $this->uri->segment(3);
        if($this->input->post('submit', TRUE) == 'submit') {
            //validasi input
            $this->form_validation->set_rules('merk', 'Merk Item', 'required|min_length[4]');
            $this->form_validation->set_rules('jenis', 'Jenis Item', 'required|min_length[4]');
            $this->form_validation->set_rules('bahan', 'Bahan Item', 'required|min_length[4]');
            $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
            $this->form_validation->set_rules('stok', 'Jumlah Stok', 'required|numeric');
            $this->form_validation->set_rules('deskripsi', 'Deskripsi Item', 'required|min_length[4]');


            if ($this->form_validation->run() == TRUE) {
                $config['upload_path'] = './asset/upload';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['file_name'] = 'gambar' . time();

                $this->load->library('upload', $config);
                $data = array(
                        'merk' => $this->input->post('merk', TRUE),
                        'jenis' => $this->input->post('jenis', TRUE),
                        'bahan' => $this->input->post('bahan', TRUE),
                        'harga' => $this->input->post('harga', TRUE),
                        'stok' => $this->input->post('stok', TRUE),
                        'deskripsi' => $this->input->post('deskripsi', TRUE)
                        // 'id' => $id
                    );

                if($this->upload->do_upload('foto')) {
                    $gbr = $this->upload->data();
                    //proses insert
                    unlink('asset/upload/'.$this->input->post('old_pict', TRUE));
                    $data['gambar'] = $gbr['file_name'];
                    $this->curl->simple_put($this->API . '/Adminapi/', $data, array(CURLOPT_BUFFERSIZE => 10));
                } else {
                   $this->curl->simple_put($this->API . '/Adminapi/', $data, array(CURLOPT_BUFFERSIZE => 10));
                }
            }
            $this->session->set_flashdata('upload', 'Data berhasil diupdate!');
            redirect('item');
        }

        $items = json_decode($this->curl->simple_get($this->API . '/Adminapi/', array("id" => $id)));
        foreach ($items as $key) {
            $data['merk'] = $key->merk;
            $data['jenis'] = $key->jenis;
            $data['bahan'] = $key->bahan;
            $data['harga'] = $key->harga;
            $data['stok'] = $key->stok;
            $data['deskripsi'] = $key->deskripsi;
            $data['gambar'] = $key->gambar_item;
        }
        $this->template->admin('admin/dashboard', 'admin/item_form', $data);
    }
}
