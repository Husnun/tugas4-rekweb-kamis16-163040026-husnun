<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->template->admin('admin/dashboard', 'admin/manage_item');
	}

    function get_data_user() {
        $list = $this->App_admin->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row[] = $no;
            $row[] = $field->merk;
            $row[] = $field->jenis;
            $row[] = $field->bahan;
            $row[] = 'Rp ' . number_format($field->harga_item,0,',','.');
            $row[] = $field->stok;
            $row[] = '<a href="'.site_url('item/delete/' . $field->id ).'" class="btn btn-danger" onclick="return confirm(\'Yakin akan menghapus?\')"><i class="fa fa-trash"></i></a><a href="'.site_url('item/update_item/' . $field->id ).'" class="btn btn-warning"><i class="fa fa-edit"></i></a><a href="'.site_url('item/detail/' . $field->id ).'" class="btn btn-success"><i class="fa fa-search-plus"></i></a>';
           
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->app_admin->count_all(),
            "recordsFiltered" => $this->app_admin->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
   
}
