-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19 Des 2018 pada 14.22
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aksesoris`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aksesoris`
--

CREATE TABLE `aksesoris` (
  `id` int(5) NOT NULL,
  `merk` varchar(15) DEFAULT NULL,
  `jenis` varchar(15) DEFAULT NULL,
  `bahan` varchar(15) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `deskripsi` varchar(150) DEFAULT NULL,
  `gambar` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aksesoris`
--

INSERT INTO `aksesoris` (`id`, `merk`, `jenis`, `bahan`, `harga`, `stok`, `deskripsi`, `gambar`) VALUES
(32, 'Deryt Martha Be', 'Ikat Pinggang', 'Cotton Polyeste', 59000, 5, 'Tampil stylish dengan belt yang dapat dipadankan dengan berbagai macam atasan. Padankan dengan kemeja dan celana slim fit serta sepatu loafer\r\n\r\nUkura', 'gambar1544264786.jpg'),
(33, 'Errun Martha Be', 'Ikat Pinggang', 'Synthetic Leath', 129000, 5, 'Tampil stylish dengan belt yang dapat dipadankan dengan berbagai macam atasan. Padankan dengan kemeja dan celana slim fit serta sepatu loafer\r\n\r\nPanja', 'gambar1544265082.jpg'),
(34, 'Xilya Melly Wat', 'Jam Tangan', 'Synthetic Leath', 129000, 5, 'Tampil gaya dengan jam tangan trendi ini. Memiliki model yang simple. Padankan dengan outfit favoritmu untuk kegiatan sehari-hari.\r\n\r\nDiameter : 3,3 c', 'gambar1544265206.jpg'),
(35, 'Garuna Martha W', 'Jam Tangan', 'Synthetic Leath', 129000, 6, 'Tampil gaya dengan mengenakan jam tangan ini! Memiliki model yang simpel. Cocok untuk dipadankan dengan tampilan casual chic kamu sehari-hari!\r\n\r\nDiam', 'gambar1544265332.jpg'),
(36, 'Forsha Martha s', 'Kacamata', '-', 69000, 5, 'Kacamata dengan bentuk yang sangat unik dan trendi cocok kamu kenakan pada gaya casualmu sehari-hari, sebagai pelindung mata\r\n\r\nLebar : 14,5 cm\r\n\r\nPer', 'gambar1544265474.jpg'),
(37, 'Gherin Martha s', 'Kacamata', '-', 129000, 6, 'Sempurnakan tampilanmu dengan aksesoris kacamata hitam trendi. Padankan dengan tampilan edgy favoritmu.\r\n\r\nLebar : 14 cm\r\n\r\nPerawatan:\r\nUntuk menjaga ', 'gambar1544265585.jpg'),
(38, 'Ceejay Martin W', 'Dompet', 'Synthetic Leath', 149000, 7, 'Tampil gaya dengan mengenakan dompet ini. Memiliki model simple dengan detail banyak kompartemen didalamnya. Cocok untuk membawa kebutuhan uang dan ka', 'gambar1544265742.jpg'),
(39, 'Cordelia Martin', 'Dompet', 'Synthetic Leath', 149000, 7, 'Tampil gaya dengan mengenakan dompet ini. Memiliki model simple dengan detail banyak kompartemen didalamnya. Cocok untuk membawa kebutuhan uang dan ka', 'gambar1544265826.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aksesoris`
--
ALTER TABLE `aksesoris`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aksesoris`
--
ALTER TABLE `aksesoris`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
