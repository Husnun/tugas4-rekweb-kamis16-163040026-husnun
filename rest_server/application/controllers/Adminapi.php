<?php

require APPPATH . '/libraries/REST_Controller.php';

class Adminapi extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Aksesoris_models", "aksesoris");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');

        if($cari != "") {
            $aksesoris = $this->aksesoris->cari($cari)->result();
        } else if ($id == "") {
            $aksesoris = $this->aksesoris->getData(null)->result();
        } else {
            $aksesoris = $this->aksesoris->getData($id)->result();
        }

        $this->response($aksesoris);
    }

    public function index_put()
    {
       
       $merk = $this->post('merk');
       $jenis = $this->post('jenis');
       $bahan = $this->post('bahan');
       $harga = $this->post('harga');
       $stok = $this->post('stok');
       $deskripsi = $this->post('deskripsi');
       $gambar = $this->post('gambar');
       $id = $this->put('id');
       
       $data = array(

            'merk' => $merk,
            'jenis' => $jenis,
            'bahan' => $bahan,
            'harga' =>$harga,
            'stok' => $stok,
            'deskripsi' => $deskripsi,
            'gambar'=> $gambar,
       );

       $update = $this->aksesoris->update('aksesoris', $data, 'id', $this->put('id'));

       if($update) {
            $this->response(array('status' => 'success', 200));
       } else {
            $this->response(array('status' => 'fail', 502));
       }
    }

    public function index_post()
    {
        $merk = $this->post('merk');
        $jenis = $this->post('jenis');
        $bahan = $this->post('bahan');
        $harga = $this->post('harga');
        $stok = $this->post('stok');
        $deskripsi = $this->post('deskripsi');
        $gambar = $this->post('gambar');
       

         $data = array(

            'merk' => $merk,
            'jenis' => $jenis,
            'bahan' => $bahan,
            'harga' =>$harga,
            'stok' => $stok,
            'deskripsi' => $deskripsi,
            'gambar'=> $gambar,
       );

        $insert = $this->aksesoris->insert($data);

        if($insert) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
       $id = $this->delete('id');
       $delete = $this->aksesoris->delete('aksesoris', 'id', $id);

       if($delete) {
            $this->response(array('status' => 'success', 200));
       } else {
            $this->response(array('status' => 'fail', 502));
       }
    }
}