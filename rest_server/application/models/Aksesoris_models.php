<?php    
class Aksesoris_models extends CI_Model {

    protected $table = 'aksesoris'; //nama tabel dari database
    protected $column_order = array(null, 'merk','jenis','bahan','harga','stok','deskripsi','gambar'); //field yang ada di table user
    protected $column_search = array('merk','jenis','bahan'); //field yang diizin untuk pencarian 
    protected $order = array('id' => 'asc'); // default order 

	public function __construct()
	{
		parent::__construct();
	}
    
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function getData($id = null){
        $this->db->select("*");
        $this->db->from("aksesoris");

        if ($id == null) {
            $this->db->order_by('id','asc');
        } else {
            $this->db->where('id', $id);
        }
        return $this->db->get();
    }

    public function cari($cari)
    {
        $this->db->select("*");
        $this->db->from("aksesoris");
        $this->db->like('merk', $cari);
        $this->db->or_like('jenis', $cari);
        $this->db->or_like('bahan', $cari);
        return $this->db->get();
    }

    public function insert($data){
       $this->db->insert('aksesoris', $data);
       return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       $this->db->where($par, $var);
       $this->db->delete($table);
       return $this->db->affected_rows();
    }
}
?>